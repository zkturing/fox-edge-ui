import request from '@/utils/request';
import {AxiosPromise} from 'axios';
import {
  DeviceModelFormData,
  DeviceModelItem,
  DeviceModelListResult,
  DeviceModelPageResult,
  DeviceModelQueryParam,
  CreateDeviceModelRequestVO
} from './types';


/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listDeviceModelEntityList(queryParams: object): AxiosPromise<DeviceModelListResult> {
  return request({
    url: '/kernel/manager/device/model/entities',
    method: 'post',
    data: queryParams,
  });
}

/**
* 获取实体分页列表
*
* @taskParam queryParams
*/
export function listDeviceModelTypeList(): AxiosPromise<string[]> {
  return request({
    url: '/kernel/manager/device/model/types',
    method: 'get'
  });
}

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listDeviceModelEntityPages(queryParams: DeviceModelQueryParam): AxiosPromise<DeviceModelPageResult> {
  return request({
    url: '/kernel/manager/device/model/page',
    method: 'post',
    data: {
      modelName: queryParams.modelName,
      modelType: queryParams.modelType,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    },
  });
}

/**
 * 获取实体详情
 *
 * @taskParam id
 */
export function getDeviceModelEntity(id: number): AxiosPromise<DeviceModelFormData> {
  return request({
    url: '/kernel/manager/device/model/entity',
    method: 'get',
    params: {id: id}
  });
}

/**
 * 添加实体
 *
 * @taskParam data
 */
export function createDeviceModelEntity(data: CreateDeviceModelRequestVO) {
  return request({
    url: '/kernel/manager/device/model/entity',
    method: 'post',
    data: data,
  });
}

/**
 * 修改实体
 *
 * @taskParam data
 */
export function updateDeviceModelEntity(data: CreateDeviceModelRequestVO) {
  return request({
    url: '/kernel/manager/device/model/entity',
    method: 'put',
    data: data,
  });
}

/**
 * 删除实体列表
 *
 * @taskParam ids
 */
export function deleteDeviceModelEntity(ids: string) {
  return request({
    url: '/kernel/manager/device/model/entities',
    method: 'delete',
    params: {ids: ids},
  });
}
