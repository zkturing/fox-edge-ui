/**
 * 实体查询参数类型
 */
export interface DeviceModelQueryParam extends PageQuery {
  modelType?: string;
  modelName?: string;
  provider?: string;
  serviceParam: object;
  extendParam?: object;
}

/**
 * 实体分页列表项
 */
export interface DeviceModelItem extends Entity{
  modelName: string;
  modelType: string;
  provider: string;
  serviceParam: object;
  serviceParamJson: string;
  modelSchema: object;
  modelSchemaJson: string;
  createTime?: number;
  updateTime?: number;
}

/**
 * 实体列表类型
 */
export type DeviceModelListResult = DeviceModelItem[];

/**
 * 实体分页项类型
 */
export type DeviceModelPageResult = PageResult<DeviceModelItem[]>;

/**
 * 实体表单类型：代表一个用于增删改查的临时对象
 */
export interface DeviceModelFormData extends DeviceModelItem {
  // 表单状态
  sort: number;
  status: number;
}

/**
 * 创建实体请求VO
 */
export interface CreateDeviceModelRequestVO {
  id?: number;
  modelName: string;
  modelType: string;
  provider: string;
  serviceParam: object;
  serviceParamJson: string;
  modelSchema: object;
  modelSchemaJson: string;
}

