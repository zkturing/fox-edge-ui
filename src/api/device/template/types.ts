/**
 * 实体查询参数类型
 */
export interface DeviceTemplateQueryParam extends PageQuery {
  fileName?: string;
  load?: boolean;
}

/**
 * 实体分页列表项
 */
export interface DeviceTemplateItem extends Entity {
  modelName: string;
  modelVersion: string;
  version: string;
  component: string;
  fileNames?: string[];
  fileName?: string;
  createTime?: number;
  updateTime?: number;
}


/**
 * 实体分页项类型
 */
export type DeviceTemplatePageResult = PageResult<DeviceTemplateItem[]>;

/**
 * 创建实体请求VO
 */
export interface CreateDeviceTemplateRequestVO {
  modelName?: string;
  modelVersion?: string;
  fileName?: string;
  version?: string;
  pathName?: string;
  component?: string;
  list?: object;
}

