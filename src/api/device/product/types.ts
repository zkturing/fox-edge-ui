import {RepositoryVerItem} from "@/api/repo/comp/file/types";

/**
 * 实体查询参数类型
 */
export interface RepoProductQueryParam extends PageQuery {
  source?: string;
  modelType?: string;
  modelName?: string;
  modelVersion?: string;
  keyWord?: string;
  component?: string;
  status?: number;
  load?: boolean;
}

/**
 * 子版本
 */
export interface RepoProductComp {
  productId?: string;
  uuid?: string;
  modelType: string;
  modelName: string;
  modelVersion: string;
  load: boolean;
  status: string;
  versions: RepositoryVerItem[];
  lastVersion: RepositoryVerItem;
}

/**
 * 实体分页列表项
 */
export interface RepoProductEntity extends Entity {
  uuid: string;
  manufacturer: string;
  deviceType: string;
  image: string;
  imageUrl?: string;
  url: string;
  tags: string;
  description: string;
  updateTime: number;
  updateTimeText: string;

  comps: RepoProductComp[];
}

/**
 * 实体分页项类型
 */
export type RepoProductPageResult = PageResult<RepoProductEntity[]>;

/**
 * 创建实体请求VO
 */
export interface CreateRepoProductRequestVO {
  modelType?: string;
  modelName?: string;
  modelVersion?: string;
  fileName?: string;
  version?: string;
  stage?: string;
  pathName?: string;
  component?: string;
  list?: object;
  lastVersion?: RepoProductComp;
}

