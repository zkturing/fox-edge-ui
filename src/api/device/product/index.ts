import {CreateRepoProductRequestVO, RepoProductEntity, RepoProductPageResult, RepoProductQueryParam,} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

export function listEntityList(queryParams: RepoProductQueryParam): AxiosPromise<RepoProductPageResult> {
  return request({
    url: '/kernel/manager/repository/product/page',
    method: 'post',
    data: {
      keyWord: queryParams.keyWord,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}

export function getEntity(uuid: string): AxiosPromise<RepoProductEntity> {
  return request({
    url: '/kernel/manager/repository/product/entity',
    method: 'get',
    params: {
      uuid: uuid,
    },
  });
}

export function downloadEntity(data: CreateRepoProductRequestVO): AxiosPromise<RepoProductPageResult> {
  return request({
    url: '/kernel/manager/repository/product/download',
    method: 'post',
    data: {
      list: data,
    },
  });
}

export function installEntity(data: CreateRepoProductRequestVO): AxiosPromise<RepoProductPageResult> {
  return request({
    url: '/kernel/manager/repository/product/install',
    method: 'post',
    data: {
      modelType: data.modelType,
      modelName: data.modelName,
      modelVersion: data.modelVersion,
      fileName: data.fileName,
      version: data.version,
      stage: data.stage,
      component: data.component,
    },
  });
}

export function scanEntityList(data: CreateRepoProductRequestVO): AxiosPromise<RepoProductPageResult> {
  return request({
    url: '/kernel/manager/repository/product/scan',
    method: 'post',
    data: {
      modelType: data.modelType,
    },
  });
}

export function deleteEntity(data: CreateRepoProductRequestVO[]): AxiosPromise<RepoProductPageResult> {
  return request({
    url: '/kernel/manager/repository/product/delete',
    method: 'post',
    data: {
      list: data,
    },
  });
}
