import request from '@/utils/request';
import {AxiosPromise} from 'axios';

export function getEnvType(): AxiosPromise<string> {
  return request({
    url: '/kernel/manager/environment/type',
    method: 'get',
  });
}
