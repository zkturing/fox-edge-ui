/**
 * 实体查询参数类型
 */
export interface RepositoryQueryParam extends PageQuery {
  source?: string;
  modelType?: string;
  modelName?: string;
  modelVersion?: string;
  keyword?: string;
  component?: string;
  status?: number;
  load?: boolean;
}

/**
 * 子版本
 */
export interface RepositoryVerItem {
  version?: string;
  stage?: string;
  component?: string;
  pathName?: string;
  description?: string;
  modelType?: string;
  modelName?: string;
  modelVersion?: string;
  fileSize?: number;
  createTime?: number;
  updateTime?: number;
}

export interface JarEntity {
  dependencies?: JarInfo[];
  property?: JarInfo;
}

export interface JarInfo {
  groupId?: string;
  artifactId?: string;
  version?: string;
}

/**
 * 实体分页列表项
 */
export interface RepositoryItem extends Entity {
  picUrl: string;
  modelType: string;
  modelName: string;
  modelVersion: string;
  fileName: string;
  version: string;
  pathName: string;
  jarEntity?: JarEntity[];
  jarEntityText?: string;
  versions?: RepositoryVerItem[];
  status: string;
  createTime?: number;
  updateTime?: number;
  lastVersion?: RepositoryVerItem;
  usedVersion?: RepositoryVerItem;
}

/**
 * 实体分页项类型
 */
export type RepositoryPageResult = PageResult<RepositoryItem[]>;

/**
 * 创建实体请求VO
 */
export interface CreateRepositoryRequestVO {
  modelType?: string;
  modelName?: string;
  modelVersion?: string;
  fileName?: string;
  version?: string;
  stage?: string;
  pathName?: string;
  component?: string;
  list?: object;
  lastVersion?: RepositoryVerItem;
}

