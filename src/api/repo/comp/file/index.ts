import {CreateRepositoryRequestVO, RepositoryPageResult, RepositoryQueryParam,} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

export function listCloudEntityList(queryParams: RepositoryQueryParam): AxiosPromise<RepositoryPageResult> {
  return request({
    url: '/kernel/manager/repository/page',
    method: 'post',
    data: {
      source: queryParams.source,
      modelType: queryParams.modelType,
      modelName: queryParams.modelName,
      modelVersion: queryParams.modelVersion,
      keyword: queryParams.keyword,
      component: queryParams.component,
      status: queryParams.status !== undefined ? Number(queryParams.status) : undefined,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}

export function downloadCloudEntity(data: CreateRepositoryRequestVO): AxiosPromise<RepositoryPageResult> {
  return request({
    url: '/kernel/manager/repository/download',
    method: 'post',
    data: {
      list: data,
    },
  });
}

export function installCloudEntity(data: CreateRepositoryRequestVO): AxiosPromise<RepositoryPageResult> {
  return request({
    url: '/kernel/manager/repository/install',
    method: 'post',
    data: {
      modelType: data.modelType,
      modelName: data.modelName,
      modelVersion: data.modelVersion,
      fileName: data.fileName,
      version: data.version,
      stage: data.stage,
      component: data.component,
    },
  });
}

export function scanCloudEntityList(data: CreateRepositoryRequestVO): AxiosPromise<RepositoryPageResult> {
  return request({
    url: '/kernel/manager/repository/scan',
    method: 'post',
    data: {
      modelType: data.modelType,
    },
  });
}

export function deleteCloudEntity(data: CreateRepositoryRequestVO[]): AxiosPromise<RepositoryPageResult> {
  return request({
    url: '/kernel/manager/repository/delete',
    method: 'post',
    data: {
      list: data,
    },
  });
}
