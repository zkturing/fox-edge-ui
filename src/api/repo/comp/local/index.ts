import {
  CreateDeviceOperateRequestVO,
  CreateRepoCompRequestVO,
  DeviceOperateItem,
  DeviceOperatePageResult,
  DeviceOperateQueryParam,
  FileTemplateItem,
  RepoCompPageResult,
  RepoCompQueryParam,
} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

export function listCompEntityList(queryParams: RepoCompQueryParam): AxiosPromise<RepoCompPageResult> {
  return request({
    url: '/kernel/manager/repository/local/comp-list/page',
    method: 'post',
    data: {
      keyWord: queryParams.keyWord,
      compRepo: queryParams.compRepo,
      compType: queryParams.compType,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}

export function createCompEntity(data: CreateRepoCompRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/comp-list/entity',
    method: 'post',
    data: data,
  });
}

export function deleteCompEntity(compId: number) {
  return request({
    url: '/kernel/manager/repository/local/comp-list/entity',
    method: 'delete',
    params: {id: compId},
  });
}

export function uploadCompEntity(data: CreateRepoCompRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/comp-list/upload',
    method: 'post',
    data: data,
  });
}

export function syncCompEntity(data: CreateRepoCompRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/comp-list/sync',
    method: 'post',
    data: data,
  });
}


export function listOperateEntityPages(queryParams: RepoCompQueryParam): AxiosPromise<DeviceOperatePageResult> {
  return request({
    url: '/kernel/manager/repository/local/operate-list/page',
    method: 'post',
    data: {
      id: queryParams.compId,
      keyWord: queryParams.keyWord,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    },
  });
}

/**
 * 添加实体
 *
 * @taskParam data
 */
export function createOperateEntity(data: CreateDeviceOperateRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/operate-list/entity',
    method: 'post',
    data: data,
  });
}

export function updateOperateEntity(data: CreateDeviceOperateRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/operate-list/entity',
    method: 'put',
    data: {
      id: data.id,
      compId: data.compId,
      manufacturer: data.manufacturer,
      deviceType: data.deviceType,
      operateName: data.operateName,
      operateMode: data.operateMode,
      dataType: data.dataType,
      serviceType: data.serviceType,
      engineType: data.engineType,
      engineParam: data.engineParam,
      polling: data.polling,
      timeout: data.timeout,
      description: data.description,
    }
  });
}

export function deleteOperateEntity(ids: string) {
  return request({
    url: '/kernel/manager/repository/local/operate-list/entities',
    method: 'delete',
    params: {ids: ids},
  });
}

export function listOperateOptionList(queryParams: DeviceOperateQueryParam): AxiosPromise<OptionType[]> {
  return request({
    url: '/kernel/manager/repository/local/operate-list/option',
    method: 'post',
    data: queryParams,
  });
}

/**
 * 获取实体详情
 *
 * @taskParam id
 */
export function getOperateEntity(id: number): AxiosPromise<DeviceOperateItem> {
  return request({
    url: '/kernel/manager/repository/local/operate-list/entity',
    method: 'get',
    params: {id: id}
  });
}


export function listFileTemplateEntities(queryParams: RepoCompQueryParam): AxiosPromise<PageResult<FileTemplateItem[]>> {
  return request({
    url: '/kernel/manager/repository/local/file-list/page',
    method: 'post',
    data: {
      id: queryParams.compId,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}

export function downloadFileTemplateEntity(compId: number, fileName: string): AxiosPromise {
  return request({
    url: '/kernel/manager/repository/local/file-list/download',
    method: 'post',
    data: {
      id: compId,
      file: fileName,
    },
    responseType: 'blob',
  });
}

export function deleteFileTemplateEntity(compId: number, fileName: string) {
  return request({
    url: '/kernel/manager/repository/local/file-list/delete',
    method: 'post',
    data: {
      id: compId,
      file: fileName,
    }
  });
}
