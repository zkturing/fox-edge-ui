import {
  CreateDeviceDecoderRequestVO,
  DeviceDecoderPageResult,
  DeviceDecoderQueryParam, DeviceVersion,
} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listDeviceDecoderEntityPages(queryParams: DeviceDecoderQueryParam): AxiosPromise<DeviceDecoderPageResult> {
  return request({
    url: '/kernel/manager/repository/local/jar-file/page',
    method: 'post',
    data: {
      fileName: queryParams.fileName,
      load: queryParams.load,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    },
  });
}

/**
 * 修改实体
 *
 * @taskParam data
 */
export function updateDeviceDecoderEntity(data: CreateDeviceDecoderRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/jar-file/entity',
    method: 'put',
    data: {
      fileName: data.fileName,
      load: data.load,
    }
  });
}

/**
 * 删除实体列表
 *
 * @taskParam ids
 */
export function deleteDeviceDecoderEntity(fileNames: string[]) {
  return request({
    url: '/kernel/manager/device/decoder/entity/delete',
    method: 'post',
    data: {
      fileName: fileNames,
    }
  });
}

/**
 * 重启服务
 *
 * @taskParam ids
 */
export function restartDeviceService() {
  return request({
    url: '/kernel/manager/repository/local/jar-file/process/restart',
    method: 'post',
    data: {applicationName: 'device-service'},
  });
}

export function getJarInfo(fileName: string): AxiosPromise<DeviceVersion> {
  return request({
    url: '/kernel/manager/repository/local/jar-file/jar-info/query',
    method: 'post',
    data: {
      fileName: fileName,
    }
  });
}
