/**
 * 实体查询参数类型
 */
export interface DeviceDecoderQueryParam extends PageQuery {
  fileName?: string;
  load?: boolean;
}

/**
 * 实体分页列表项
 */
export interface DeviceDecoderItem extends Entity {
  packName: string;
  jarVer: string;
  first: DeviceVersion;
  versions: DeviceVersion[];

  fileName: string;
  load: boolean;
  groupId: string;
  artifactId: string;
  version: string;
  jarSpace: string;
  size: number;
  createTime: number;
  updateTime: number;
  dependencies: property[];
  classFileName: string[];
}

export interface DeviceVersion {
  jarVer: string;
  fileName: string;
  fileNameShow: string;
  load: boolean;
  groupId: string;
  groupIdShow: string;
  artifactId: string;
  artifactIdShow: string;
  version: string;
  jarSpace: string;
  size: number;
  createTime: number;
  updateTime: number;
  dependencies: property[];
  classFileName: string[];
  classFileNameShow: string[];
}

export interface property {
  groupId: string;
  artifactId: string;
  artifactIdShow: string;
  version: string;
}

/**
 * 实体列表类型
 */
export type DeviceDecoderListResult = DeviceDecoderItem[];

/**
 * 实体分页项类型
 */
export type DeviceDecoderPageResult = PageResult<DeviceDecoderItem[]>;

/**
 * 实体表单类型：代表一个用于增删改查的临时对象
 */
export interface DeviceDecoderFormData extends DeviceDecoderItem {
  // 表单状态
  sort: number;
  status: number;
}

/**
 * 创建实体请求VO
 */
export interface CreateDeviceDecoderRequestVO {
  fileName: string;
  load: boolean;
}

