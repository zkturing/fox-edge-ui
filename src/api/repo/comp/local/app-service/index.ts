import {ProcessStatusListResult, ServiceStatusItemListResult,} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';
import {CreateRepoCompRequestVO} from "@/api/repo/comp/local/types";

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listProcessStatusList(): AxiosPromise<ProcessStatusListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/process/entities',
    method: 'get'
  });
}

/**
 * 重启进程
 *
 * @taskParam queryParams
 */
export function restartProcess(appName: string, appType: string): AxiosPromise<ProcessStatusListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/process/restart',
    method: 'post',
    data: {
      appName: appName,
      appType: appType
    },
  });
}

/**
 * 重启进程
 *
 * @taskParam queryParams
 */
export function stopProcess(appName: string, appType: string): AxiosPromise<ProcessStatusListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/process/stop',
    method: 'post',
    data: {
      appName: appName,
      appType: appType
    },
  });
}

export function uninstallProcess(appName: string, appType: string): AxiosPromise<ProcessStatusListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/process/uninstall',
    method: 'post',
    data: {
      appName: appName,
      appType: appType
    },
  });
}

/**
 * 重启进程
 *
 * @taskParam queryParams
 */
export function updateLoadConfig(appName: string, appType: string, appLoad: boolean): AxiosPromise<ProcessStatusListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/config/load',
    method: 'post',
    data: {
      appName: appName,
      appType: appType,
      appLoad: appLoad,
    },
  });
}

/**
 * GC 进程
 *
 * @taskParam queryParams
 */
export function gcProcess(pid: number): AxiosPromise<ProcessStatusListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/process/gc',
    method: 'get',
    params: {pid: pid},
  });
}

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listServiceStatusList(): AxiosPromise<ServiceStatusItemListResult> {
  return request({
    url: '/kernel/manager/repository/local/app-service/status/entities',
    method: 'get'
  });
}

export function updateConfFile(data: CreateRepoCompRequestVO) {
  return request({
    url: '/kernel/manager/repository/local/app-service/conf',
    method: 'put',
    data: data,
  });
}
