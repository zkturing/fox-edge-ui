/**
 * 实体查询参数类型
 */
export interface RepoCompQueryParam extends PageQuery {
  compId?: number;
  compRepo?: string;
  compType?: string;
  file?: string;
  keyWord?: string;
}


/**
 * 实体分页列表项
 */
export interface RepoCompEntity extends Entity {
  compName: string;
  compParam: object;
  compParamJson: string;
  compParamShow: object;
  compParamShowJson: string;
  compRepo: string;
  compType: string;
}

export interface FileTemplateItem extends Entity {
  component: string;
  manufacturer: string;
  deviceType: string;
  fileName?: string;
  createTime?: number;
  updateTime?: number;
}

/**
 * 实体分页项类型
 */
export type RepoCompPageResult = PageResult<RepoCompEntity[]>;

/**
 * 创建实体请求VO
 */
export interface CreateRepoCompRequestVO {
  id?: number;
  compRepo?: string;
  compType?: string;
  compParam?: any;
}

/**
 * 实体查询参数类型
 */
export interface DeviceOperateQueryParam extends PageQuery {
  manufacturer?: string;
  deviceType?: string;
  operateName?: string;
  operateMode?: string;
  dataType?: string;
  serviceType?: string;
  engineType?: string;
  engineParam?: ScriptOperateMethod;
  polling?: boolean;
  timeout?: number;
}

export interface ScriptOperateMethod {
  description?: string;
  encode?: ScriptMethod;
  decode?: ScriptMethod;
}


export interface ScriptMethod {
  main: string;
  code: string;
}

/**
 * 实体分页列表项
 */
export interface DeviceOperateItem extends Entity {
  manufacturer: string;
  deviceType: string;
  operateName: string;
  operateMode: string;
  dataType: string;
  serviceType: string;
  engineType: string;
  engineParam: ScriptOperateMethod;
  polling: boolean;
  timeout: number;
  createTime?: number;
  updateTime?: number;
}


/**
 * 实体分页项类型
 */
export type DeviceOperatePageResult = PageResult<DeviceOperateItem[]>;

/**
 * 创建实体请求VO
 */
export interface CreateDeviceOperateRequestVO {
  id?: number;
  compId?: number;
  manufacturer: string;
  deviceType: string;
  operateName: string;
  operateMode: string;
  dataType: string;
  serviceType: string;
  engineType: string;
  engineParam?: ScriptOperateMethod;
  polling: boolean;
  pollingTxt: string;
  timeout: number;
  description?: string;
}
