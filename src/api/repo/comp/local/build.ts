import {ScriptMethod, ScriptOperateMethod} from "./types";

/**
 * 刷新表单
 */
export function buildEngineParamDefaultValue(operateMode: string) {
  const engineParam = {} as ScriptOperateMethod;

  if (operateMode === 'exchange') {
    engineParam.encode = buildEncodeDefault();
    engineParam.decode = buildDecodeDefault();
  }
  if (operateMode === 'publish') {
    engineParam.encode = buildEncodeDefault();
  }
  if (operateMode === 'report') {
    engineParam.decode = buildDecodeDefault();
  }
  if (operateMode === 'keyHandler') {
    engineParam.decode = buildKeyHandlerDefault();
  }
  if (operateMode === 'splitHandler') {
    engineParam.decode = buildSplitHandlerDefault();
  }

  return engineParam;
}

export function buildCode(main: string, code: string) {
  const decode = {} as ScriptMethod;

  if (decode.main === undefined) {
    decode.main = main;
  }
  if (decode.code === undefined) {
    decode.code = code;
  }

  return decode;
}

export function buildEncodeDefault() {
  return buildCode('encode', 'function encode(param)\r\n{\r\n return "";\r\n}');
}

export function buildDecodeDefault() {
  return buildCode('decode', 'function decode(recv,param)\r\n{\r\nvar object=JSON.parse(param);\r\nreturn JSON.stringify(object)\r\n}');
}

export function buildKeyHandlerDefault() {
  return buildCode('decode', 'function decode(recv,param)\r\n{\r\nvar object=JSON.parse(param);\r\nreturn JSON.stringify(object)\r\n}');
}

export function buildSplitHandlerDefault() {
  return buildCode('decode', 'function decode(recv,param)\r\n{\r\nvar object=JSON.parse(param);\r\nreturn JSON.stringify(object)\r\n}');
}
