/**
 * 实体查询参数类型
 */
export interface RepoCompQueryParam extends PageQuery {
  keyWord?: string;
}


/**
 * 实体分页列表项
 */
export interface RepoCompEntity extends BaseModel {
  ownerId: string;
  groupName: string;
  manufacturer: string;
  deviceType: string;
  description: string;
  commitKey: string;
  versionIds: string[];
  weight: number;
}

/**
 * 实体分页项类型
 */
export type RepoCompPageResult = PageResult<RepoCompEntity[]>;

/**
 * 创建实体请求VO
 */
export interface CreateRepoCompRequestVO {
  ownerId: string;
  groupName: string;
  manufacturer: string;
  deviceType: string;
  description: string;
  commitKey: string;
  versionIds: string[];
  weight: number;
}

