/**
 * 实体查询参数类型
 */
export interface RepoCompVersionQueryParam extends PageQuery {
  scriptId?: string;
  versionId?: string;
  keyword?: string;
}


/**
 * 实体分页列表项
 */
export interface RepoCompVersionEntity extends BaseModel {
  manufacturer?: string;
  deviceType?: string;
  author?: string;
  scriptId?: string;
  description?: string;
  operates: RepoCompOperateEntity[];
}

/**
 * 实体分页列表项
 */
export interface RepoCompOperateEntity {
  versionId?: string;
  manufacturer?: string;
  deviceType?: string;
  operateName?: string;
  timeout?: number;
  serviceType?: string;
  operateMode?: string;
  dataType?: string;
  engineParam: ScriptOperateMethod;
  createTime: number;
  updateTime: number;
}

export interface ScriptOperateMethod {
  encode?: ScriptMethod;
  decode?: ScriptMethod;
}

export interface ScriptMethod {
  main: string;
  code: string;
}

/**
 * 实体分页项类型
 */
export type RepoCompVersionPageResult = PageResult<RepoCompVersionEntity[]>;

/**
 * 创建实体请求VO
 */
export interface CreateRepoCompVersionRequestVO {
  ownerId: string;
  groupName: string;
  manufacturer: string;
  deviceType: string;
  description: string;
  commitKey: string;
  versionIds: string[];
  weight: number;
}

