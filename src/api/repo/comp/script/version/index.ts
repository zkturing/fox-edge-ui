import {RepoCompVersionEntity, RepoCompVersionPageResult, RepoCompVersionQueryParam,} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

export function listRepoCompVersionEntityList(queryParams: RepoCompVersionQueryParam): AxiosPromise<RepoCompVersionPageResult> {
  return request({
    url: '/kernel/manager/repository/script/version/page',
    method: 'post',
    data: {
      scriptId: queryParams.scriptId,
      keyword: queryParams.keyword,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}

export function listRepoCompOperateEntityList(queryParams: RepoCompVersionQueryParam): AxiosPromise<RepoCompVersionEntity[]> {
  return request({
    url: '/kernel/manager/repository/script/version/operate/entities',
    method: 'post',
    data: {
      versionId: queryParams.versionId,
      keyword: queryParams.keyword,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}

export function getRepoCompOperateEntity(versionId: string, operateId: string) {
  return request({
    url: '/kernel/manager/repository/script/version/operate/entity',
    method: 'post',
    data: {
      versionId: versionId,
      operateId: operateId,
    },
  });
}

export function installRepoCompOperateEntity(scriptId: string, versionId: string) {
  return request({
    url: '/kernel/manager/repository/script/version/install',
    method: 'post',
    data: {
      scriptId: scriptId,
      versionId: versionId,
    },
  });
}
