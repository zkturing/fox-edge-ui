import {RepoCompPageResult, RepoCompQueryParam,} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

export function listEntityList(queryParams: RepoCompQueryParam): AxiosPromise<RepoCompPageResult> {
  return request({
    url: '/kernel/manager/repository/script/page',
    method: 'post',
    data: {
      keyword: queryParams.keyWord,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    }
  });
}
