import {
  CreateExtendRequestVO,
  ExtendFormData,
  ExtendListResult,
  ExtendPageResult,
  ExtendQueryParam,
} from './types';
import request from '@/utils/request';
import {AxiosPromise} from 'axios';

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listExtendEntityList(queryParams: object): AxiosPromise<ExtendListResult> {
  return request({
    url: '/kernel/manager/extend/entities',
    method: 'post',
    data: queryParams,
  });
}

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listExtendEntityPages(queryParams: ExtendQueryParam): AxiosPromise<ExtendPageResult> {
  return request({
    url: '/kernel/manager/extend/page',
    method: 'post',
    data: {
      extendType: queryParams.extendType,
      extendName: queryParams.extendName,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    },
  });
}

/**
 * 获取实体详情
 *
 * @taskParam id
 */
export function getExtendEntity(id: number): AxiosPromise<ExtendFormData> {
  return request({
    url: '/kernel/manager/extend/entity',
    method: 'get',
    params: {id: id}
  });
}

/**
 * 添加实体
 *
 * @taskParam data
 */
export function createExtendEntity(data: CreateExtendRequestVO) {
  return request({
    url: '/kernel/manager/extend/entity',
    method: 'post',
    data: data,
  });
}

/**
 * 修改实体
 *
 * @taskParam data
 */
export function updateExtendEntity(data: CreateExtendRequestVO) {
  return request({
    url: '/kernel/manager/extend/entity',
    method: 'put',
    data: {
      id: data.id,
      extendType: data.extendType,
      extendName: data.extendName,
      extendParam: data.extendParam
    }
  });
}

/**
 * 删除实体列表
 *
 * @taskParam ids
 */
export function deleteParamTemplateEntity(ids: string) {
  return request({
    url: '/kernel/manager/extend/entities',
    method: 'delete',
    params: {ids: ids},
  });
}
