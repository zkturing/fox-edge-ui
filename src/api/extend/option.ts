import {formatOptionLabel} from "@/utils/formatter";


/**
 * 获取选项列表
 * @taskParam field 字段类型
 */
export function listOptionList(field: string): OptionType[] {
  const options = [] as OptionType[];

  if (field === 'extendType') {
    options.push({value: 'DeviceEntityGlobe', label: '设备：全体级别'});
    options.push({value: 'DeviceEntityType', label: '设备：类型级别'});
    options.push({value: 'DeviceEntityObject', label: '设备：设备级别'});

    options.push({value: 'DeviceMapperEntityGlobe', label: '设备映射：全体级别'});
    options.push({value: 'DeviceMapperEntityType', label: '设备映射：类型级别'});
  }

  return options;
}

/**
 * 获得option的文本
 * @taskParam optionType option的类型
 * @taskParam optionValue option的数值
 */
export function getSelectLabel(optionType: any, optionValue: any): string {
  const options = listOptionList(optionType);
  return formatOptionLabel(options, optionValue);
}

/**
 * 获得option的文本
 * @taskParam row 行信息
 * @taskParam column 列信息
 */
export function getOptionLabel(row: any, column: any): string {
  const options = listOptionList(column.property);
  return formatOptionLabel(options, row[column.property]);
}
