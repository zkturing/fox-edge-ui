/**
 * 实体查询参数类型
 */
export interface ExtendQueryParam extends PageQuery {
  extendType?: string;
  extendName?: string;
}

/**
 * 实体分页列表项
 */
export interface ExtendItem extends Entity {
  extendType?: string;
  extendName?: string;
  extendParam?: object;
  extendParamJson?: string;
  createTime?: number;
  updateTime?: number;
}

/**
 * 实体列表类型
 */
export type ExtendListResult = ExtendItem[];

/**
 * 实体分页项类型
 */
export type ExtendPageResult = PageResult<ExtendItem[]>;

/**
 * 实体表单类型：代表一个用于增删改查的临时对象
 */
export interface ExtendFormData extends ExtendItem {
  // 表单状态
  sort: number;
  status: number;
}

/**
 * 创建实体请求VO
 */
export interface CreateExtendRequestVO {
  id?: number;
  extendType?: string;
  extendName?: string;
  extendParam?: object;
  extendParamJson?: string;
}

