import request from '@/utils/request';
import {AxiosPromise} from 'axios';


/**
 * 获取选项列表：查询entityType表的field字段，并对该字段做一个distinct操作，把结果作为选项返回。
 * 例如：listOptionList("ChannelEntity", "channelType")
 * 意思是查询ChannelEntity表，将channelType字段作为选项
 *
 * @taskParam entityType 实体类型
 * @taskParam field
 */
export function listOptionList(entityType: string, field: string): AxiosPromise<OptionType[]> {
  return request({
    url: '/kernel/manager/option/data',
    method: 'post',
    data: {
      mode: 'Option1',
      entityType: entityType,
      field: field
    },
  });
}

/**
 * 获取选项列表：查询entityType表的field2字段，按field1字段=value1的条件进行过滤
 * 例如：listOption2List("OperateEntity", "manufacturer", '武汉中科图灵', 'deviceType')
 * 意思是查询OperateEntity表，按将manufacturer=武汉中科图灵的条件进行过滤，并将查询结果中的deviceType字段作为选项返回
 *
 * @taskParam entityType 实体类型
 * @taskParam field
 */
export function listOption2List(entityType: string, field1: string, value1: string, field2: string): AxiosPromise<OptionType[]> {
  return request({
    url: '/kernel/manager/option/data',
    method: 'post',
    data: {
      mode: 'Option2',
      entityType: entityType,
      field: field1,
      value: value1,
      field2: field2
    },
  });
}

/**
 * 获取选项列表：对entityType的field1，field2进行distinct操作，过滤条件为field1=value1，并返回两者中的一个字段（value来指明）字段的内容为选项
 * @taskParam entityType 实体类型
 * @taskParam field
 */
export function listOption3ist(entityType: string, field1: string, value1: string, field2: string, value: string, label: string): AxiosPromise<OptionType[]> {
  return request({
    url: '/kernel/manager/option/data',
    method: 'post',
    data: {
      mode: 'Option3',
      entityType: entityType,
      field1: field1,
      value1: value1,
      field2: field2,
      value: value,
      label: label
    },
  });
}
