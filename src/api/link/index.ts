import request from '@/utils/request';
import {AxiosPromise} from 'axios';
import {
  LinkFormData, LinkItem,
  LinkListResult,
  LinkPageResult,
  LinkQueryParam,
  CreateLinkRequestVO
} from './types';


/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listLinkEntityList(queryParams: object): AxiosPromise<LinkListResult> {
  return request({
    url: '/kernel/manager/link/entities',
    method: 'post',
    data: queryParams,
  });
}

/**
* 获取实体分页列表
*
* @taskParam queryParams
*/
export function listLinkTypeList(): AxiosPromise<string[]> {
  return request({
    url: '/kernel/manager/link/types',
    method: 'get'
  });
}

/**
 * 获取实体分页列表
 *
 * @taskParam queryParams
 */
export function listLinkEntityPages(queryParams: LinkQueryParam): AxiosPromise<LinkPageResult> {
  return request({
    url: '/kernel/manager/link/page',
    method: 'post',
    data: {
      linkName: queryParams.linkName,
      linkType: queryParams.linkType,
      pageNum: queryParams.pageNum,
      pageSize: queryParams.pageSize
    },
  });
}

/**
 * 获取实体详情
 *
 * @taskParam id
 */
export function getLinkEntity(id: number): AxiosPromise<LinkFormData> {
  return request({
    url: '/kernel/manager/link/entity',
    method: 'get',
    params: {id: id}
  });
}

/**
 * 添加实体
 *
 * @taskParam data
 */
export function createLinkEntity(data: CreateLinkRequestVO) {
  return request({
    url: '/kernel/manager/link/entity',
    method: 'post',
    data: data,
  });
}

/**
 * 修改实体
 *
 * @taskParam data
 */
export function updateLinkEntity(data: CreateLinkRequestVO) {
  return request({
    url: '/kernel/manager/link/entity',
    method: 'put',
    data: data,
  });
}

/**
 * 删除实体列表
 *
 * @taskParam ids
 */
export function deleteLinkEntity(ids: string) {
  return request({
    url: '/kernel/manager/link/entities',
    method: 'delete',
    params: {ids: ids},
  });
}
