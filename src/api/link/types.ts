/**
 * 实体查询参数类型
 */
export interface LinkQueryParam extends PageQuery {
  linkType?: string;
  linkName?: string;
  linkParam?: string;
  extendParam?: string;
}

/**
 * 实体分页列表项
 */
export interface LinkItem extends Entity{
  linkName: string;
  linkType: string;
  linkParam: object;
  linkParamJson: string;
  extendParam: object;
  extendParamJson: string;
  createTime?: number;
  updateTime?: number;
}

/**
 * 实体列表类型
 */
export type LinkListResult = LinkItem[];

/**
 * 实体分页项类型
 */
export type LinkPageResult = PageResult<LinkItem[]>;

/**
 * 实体表单类型：代表一个用于增删改查的临时对象
 */
export interface LinkFormData extends LinkItem {
  // 表单状态
  sort: number;
  status: number;
}

/**
 * 创建实体请求VO
 */
export interface CreateLinkRequestVO {
  id?: number;
  linkName: string;
  linkType: string;
  linkParam: object;
  linkParamJson: string;
  extendParam: object;
  extendParamJson: string;
}

