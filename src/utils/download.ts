import {AxiosResponse} from "axios";


export function downloadFile(response: AxiosResponse, fileName: string): any {
  // 文件下载
  console.info("文件下载")
  const blob = new Blob([response.data], {
    type: "application/file",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");

  a.href = url;

  // 如果用户输入的自己的名称，那么就用用户输入的名称
  if (fileName !== "" && fileName !== undefined && fileName !== null) {
    a.download = fileName;
  }
  // 否则使用后台的文件名称
  else {
    const headerFilename = response.headers["content-disposition"]
      ?.split(";")[1]
      .split("=")[1];
    a.download = headerFilename as string;
  }

  a.click();
}
