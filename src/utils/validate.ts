/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @extend {string} path
 * @returns {Boolean}
 */
export function isExternal(path: string) {
  const isExternal = /^(https?:|http?:|mailto:|tel:)/.test(path);
  return isExternal;
}
